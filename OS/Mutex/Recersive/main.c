#include<stdio.h>
# define __USE_GNU
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
//#include<semaphore.h>

int count=0;
pthread_mutex_t countmutex=PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;

void* incthread(void* data)
{
	while(1)
	{
		pthread_mutex_lock(&countmutex);
		pthread_mutex_lock(&countmutex);
		count++;
		printf("incthread: %d \n",count);
		pthread_mutex_unlock(&countmutex);
		pthread_mutex_unlock(&countmutex);
	}
}

void* decthread(void *data)
{
	while(1)
	{
		pthread_mutex_lock(&countmutex);
		pthread_mutex_lock(&countmutex);
		count--;
		printf("decthread: %d \n",count);
		pthread_mutex_unlock(&countmutex);
		pthread_mutex_unlock(&countmutex);
	}
}

int main()
{
pthread_t idinc,iddec;

//pthread_mutex_recersive_init(&countmutex,NULL);
pthread_create(&idinc,NULL,incthread,NULL);
pthread_create(&iddec,NULL,decthread,NULL);

pthread_join(idinc,NULL);
pthread_join(iddec,NULL);
pthread_mutex_destroy(&countmutex);


return 0;
}
