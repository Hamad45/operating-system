#include<stdio.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<semaphore.h>

int count=0;
pthread_mutex_t countmutex;

void* incthread(void* data)
{
	while(1)
	{
	pthread_mutex_lock(&countmutex);
	count++;
	printf("incthread: %d \n",count);
	pthread_mutex_unlock(&countmutex);
	}
}

void* decthread(void *data)
{
	while(1)
	{
	pthread_mutex_lock(&countmutex);
	count--;
	printf("decthread: %d \n",count);
	pthread_mutex_unlock(&countmutex);
	}
}

int main()
{
pthread_t idinc,iddec;

pthread_mutex_init(&countmutex,NULL);
pthread_create(&idinc,NULL,incthread,NULL);
pthread_create(&iddec,NULL,decthread,NULL);
pthread_mutex_destroy(&countmutex);

pthread_join(idinc,NULL);
pthread_join(iddec,NULL);


return 0;
}
