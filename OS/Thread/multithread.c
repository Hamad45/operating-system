#include<stdio.h>
#include<pthread.h>

void* thread1(void* data)
{
printf("Welcome This is thread \n");
}

void* thread2(void* data)
{
printf("Bye This is thread \n");
}


int main(int argc,const char *argv[])
{
pthread_t tid1,tid2;
printf("Creating thread \n");
pthread_create(&tid1,NULL,thread1,NULL);
pthread_join(tid1,NULL);
pthread_create(&tid2,NULL,thread2,NULL);
pthread_join(tid2,NULL);
printf("Created thread \n");
return 0;
}
