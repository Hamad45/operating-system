#include "doublell.h"

struct node{
	struct node *prev;
	int info;
	struct node *next;
};

struct node *insert_beg(struct node *start,int data)
{
struct node *temp;
temp=(struct node *)malloc(sizeof(struct node));
temp->info=data;
temp->prev=NULL;
temp->next=start;
start=temp;
return start;
}

