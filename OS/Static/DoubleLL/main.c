#include<stdio.h>
#include<stdlib.h>
struct node{
	struct node *prev;
	int info;
	struct node *next;
};
void display(struct node *start);
void search(struct node *start,int item);
struct node *insert_beg(struct node *start,int data);
struct node *insert_end(struct node *start,int data);
struct node *delete_beg(struct node *start,int data);
struct node *delete_end(struct node *start,int data);

int main()
{
	struct node *start=NULL;
	int choice,data,item,pos;
	while(1)
	{
		printf("1.Display\n2.Search\n3.Insert at beginning\n4.Insert at end\n5.Delete at beginning\n6.Delete at end\n7.Exit\n");
		printf("enter your choice:");
		scanf("%d",&choice);
		switch(choice)
		{
		
			case 1: display(start);
				break;
			case 2: printf("enter the element to be searched");
				scanf("%d",&item);
				search(start,item);
				break;
			case 3: printf("enter the element to be inserted");
				scanf("%d",&data);
				start=insert_beg(start,data);
				break;
			case 4: printf("enter the element to be inserted");
				scanf("%d",&data);
				start=insert_end(start,data);
				break;
			case 5:	printf("enter the element to be deleted");
				scanf("%d",&data);
				start=delete_beg(start,data);
				break;
			case 6:	printf("enter the element to be deleted");
				scanf("%d",&data);
				start=delete_end(start,data);
				break;
			case 7: exit(1);
			default : printf("wrong choice\n");
		}
	}
}


