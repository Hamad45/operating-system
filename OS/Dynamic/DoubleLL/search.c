#include "doublell.h"

struct node{
	struct node *prev;
	int info;
	struct node *next;
};

void search(struct node *start,int item)
{
	struct node *p=start;
	int pos=1;
	while(p!=NULL)
	{
	if(p->info==item)
	{
		printf("item %d found at position %d\n",item,pos);
		return;
	}
	p=p->next;
	pos++;
	}
	printf("item %d not found in list\n",item);
}


