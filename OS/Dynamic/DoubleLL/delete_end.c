#include "doublell.h"

struct node{
	struct node *prev;
	int info;
	struct node *next;
};

struct node *delete_end(struct node *start,int data)
{
struct node *temp;
	temp=start->next;
	while(temp->next!=NULL)
	{
		if(temp->info==data)
		{
		temp->prev->next=NULL;
		free(temp);
		return start;
		}
	}
	printf("element %d not found\n",data);
	return start;
}
