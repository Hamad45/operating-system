#include "doublell.h"

struct node{
	struct node *prev;
	int info;
	struct node *next;
};
struct node *insert_end(struct node *start,int data)
{
struct node *p,*temp;
	temp=(struct node *)malloc(sizeof(struct node));
	temp->info=data;
	p=start;
	while(p->next != NULL);
		p=p->next;
	p->next=temp;
	temp->next=NULL;
	temp->prev=p;
	return start;
}
