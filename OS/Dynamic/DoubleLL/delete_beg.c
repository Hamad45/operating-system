#include "doublell.h"

struct node{
	struct node *prev;
	int info;
	struct node *next;
};

struct node *delete_beg(struct node *start,int data)
{
	struct node *temp;
	if(start==NULL)
	{
		printf("list is empty\n");
		return start;
	}
	if(start->info==data)
	{
		temp=start;
		start=start->next;
		start->prev=NULL;
		free(temp);
	}
	return start;
}
